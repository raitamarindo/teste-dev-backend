# Backend Developer Assessment - Dito
An REST API with event autocomplete and query for transactions timeline.

### Prerequisites

 * [Node.js 10.12 or above](https://nodejs.org/en/download/)
 * [MongoDB](https://www.mongodb.com/download-center/community) 

### Quick start

 1. Download or clone this repository
 2. Install dependencies: `npm install`
 3. Setup application: `npm run setup`

    Fill database connection data. Sample:
    ```
    ? Enter database host localhost
    ? Enter database port 27017
    ? Enter database user name dito
    ? Enter database password ****
    ? Enter database name dito-events
    Loading initial event data...
    5 events stored on database.
    OK: Setup successful!
    ```
    At the end of setup, initial event data in [https://storage.googleapis.com/dito-questions/events.json](https://storage.googleapis.com/dito-questions/events.json) are loaded to database.
    Note that in this example, the user "dito" must already exist previously with permission to access the database "dito-events". The MongoDB server must require user authentication.
 4. Run server: `npm start`

    The application should be running on 3000 port.

### API Reference

**POST /api/events** (Register a new event)
```
{
    "event":"comprou-produto",
    "timestamp":"2016-09-22T13:57:32.2311892-03:00",
    "custom_data":[
        { "key":"product_name", "value":"Camisa Azul" },
        { "key":"transaction_id","value":"3029384" },
        { "key":"product_price","value":100 }
    ]
}
```
Response:
```
[
    {
        "_id": "5c8f8200728ad8320c30ca25",
        "event": "comprou-produto",
        "timestamp": "2016-09-22T16:57:32.231Z",
        "custom_data": [
            {
                "key": "product_name",
                "value": "Camisa Azul"
            },
            {
                "key": "transaction_id",
                "value": "3029384"
            },
            {
                "key": "product_price",
                "value": 100
            }
        ],
        "createdAt": "2019-03-18T11:33:20.035Z",
        "__v": 0
    }
]
```

**POST /api/events** (Register a list of events)
```
[
    {
        "event":"comprou",
        "timestamp":"2016-09-22T13:57:31.2311892-03:00",
        "revenue":250,
        "custom_data":[
            { "key":"store_name","value":"Patio Savassi" },
            { "key":"transaction_id","value":"3029384" }
        ]
    },
    {
        "event":"comprou-produto",
        "timestamp":"2016-09-22T13:57:33.2311892-03:00",
        "custom_data":[
            { "key":"product_price","value":150 },
            { "key":"transaction_id","value":"3029384" },
            { "key":"product_name","value":"Calça Rosa" }
        ]
    }
]
```
Response:
```
[
    {
        "_id": "5c8f8246728ad8320c30ca26",
        "event": "comprou",
        "timestamp": "2016-09-22T16:57:31.231Z",
        "revenue": 250,
        "custom_data": [
            {
                "key": "store_name",
                "value": "Patio Savassi"
            },
            {
                "key": "transaction_id",
                "value": "3029384"
            }
        ],
        "createdAt": "2019-03-18T11:34:30.143Z",
        "__v": 0
    },
    {
        "_id": "5c8f8246728ad8320c30ca27",
        "event": "comprou-produto",
        "timestamp": "2016-09-22T16:57:33.231Z",
        "custom_data": [
            {
                "key": "product_price",
                "value": 150
            },
            {
                "key": "transaction_id",
                "value": "3029384"
            },
            {
                "key": "product_name",
                "value": "Calça Rosa"
            }
        ],
        "createdAt": "2019-03-18T11:34:30.143Z",
        "__v": 0
    }
]
```

**GET /api/events/5c8f8200728ad8320c30ca25** (Get an event by id)

Response:
```
{
    "_id": "5c8f8246728ad8320c30ca27",
    "event": "comprou-produto",
    "timestamp": "2016-09-22T16:57:33.231Z",
    "custom_data": [
        {
            "key": "product_price",
            "value": 150
        },
        {
            "key": "transaction_id",
            "value": "3029384"
        },
        {
            "key": "product_name",
            "value": "Calça Rosa"
        }
    ],
    "createdAt": "2019-03-18T11:34:30.143Z",
    "__v": 0
}
```

**GET /api/events?event=com&page=0&limit=2** (Paginated search of events)

Response:
```
{
    "entities": [
        {
            "_id": "5c8f25b7976c9f1a44ad1493",
            "event": "comprou-produto",
            "timestamp": "2016-09-22T16:57:32.231Z"
        },
        {
            "_id": "5c8f25b7976c9f1a44ad1494",
            "event": "comprou",
            "timestamp": "2016-09-22T16:57:31.231Z"
        }
    ],
    "filter": {
        "event": "com",
        "page": 0,
        "limit": 2
    },
    "total": 13
}
```

**GET /api/events/autocomplete/com** (Autocomplete event name)

Response:
```
[
    "comprou",
    "comprou-produto"
]
```

**GET /api/transactions/timeline** (Get a list of all transactions)

Response:
```
{
    "timeline": [
        {
            "products": [
                {
                    "name": "Tenis Preto",
                    "price": 120
                }
            ],
            "timestamp": "2016-10-02T14:37:31.230Z",
            "revenue": 120,
            "transaction_id": "3409340",
            "store_name": "BH Shopping"
        },
        {
            "products": [
                {
                    "price": 150,
                    "name": "Calça Rosa"
                },
                {
                    "name": "Camisa Azul",
                    "price": 100
                }
            ],
            "timestamp": "2016-09-22T16:57:33.231Z",
            "revenue": 250,
            "transaction_id": "3029384",
            "store_name": "Patio Savassi"
        }
    ]
}
```
