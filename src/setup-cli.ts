// tslint:disable-next-line:no-import-side-effect
import 'reflect-metadata';
import * as commander from 'commander';
import * as inquirer from 'inquirer';
import * as request from 'request';
import {
    AppConfig,
    IDatabaseConfig,
} from './config/app.config';
import { Event } from './entities';
import { container } from './inversify.config';
import {
    EventService,
    SetupService,
} from './services';

const setupService: SetupService = container.get<SetupService>(SetupService);
const eventService: EventService = container.get<EventService>(EventService);
const databaseConfig: IDatabaseConfig = AppConfig.getDatabase() || {
    host: null,
    port: null,
    database: null,
    username: null,
    password: null,
};
const questions: inquirer.Questions = [
    {
        type: 'input',
        name: 'databaseHost',
        message: 'Enter database host',
        default: databaseConfig.host || 'localhost',
    },
    {
        type: 'input',
        name: 'databasePort',
        message: 'Enter database port',
        default: databaseConfig.port || 27017,
        filter: (port: any) => parseInt(port, 10),
        validate: (port: any) => !isNaN(port),
    },
    {
        type: 'input',
        name: 'databaseUsername',
        message: 'Enter database user name',
        default: databaseConfig.username,
        validate: (username: string) => !!username,
    },
    {
        type: 'password',
        name: 'databasePassword',
        message: 'Enter database password',
        default: databaseConfig.password,
        validate: (password: string) => !!password,
        mask: '*',
    },
    {
        type: 'input',
        name: 'databaseName',
        message: 'Enter database name',
        default: databaseConfig.database,
        validate: (database: string) => !!database,
    },
];

async function startSetup() {
    const answers: inquirer.Answers = await inquirer.prompt(questions);

    try {
        await setupService.setup({
            database: {
                host: answers.databaseHost,
                port: answers.databasePort,
                username: answers.databaseUsername,
                password: answers.databasePassword,
                database: answers.databaseName,
            },
        });

        // tslint:disable-next-line: no-console
        console.info('Loading initial event data...');
        request(
            'https://storage.googleapis.com/dito-questions/events.json',
            { json: true },
            (_err: any, _res: request.Response, body: { events: Event[] }) => {

                eventService.create(body.events)
                    .then((e: Event[]) => {
                        // tslint:disable-next-line: no-console
                        console.info(`${e.length} events stored on database.`)
                        // tslint:disable-next-line: no-console
                        console.info('OK: Setup successful!');
                        process.exit();
                    })
                    .catch((err: any) => {
                        throw err;
                    });
            },
        );
    } catch (err) {
        const message: string = typeof err === 'string' ? err : err.message;
        // tslint:disable-next-line: no-console
        console.error(`ERROR: ${message}`);
        process.exit(1);
    }
}

commander
    .description('Setup TShirtShop API Application')
    .command('start')
    .description('Starts the application setup.')
    .action(startSetup);

commander.parse(process.argv);
