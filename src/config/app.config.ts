import { Config } from './config';

export interface IDatabaseConfig {
    host: string;
    port: number;
    database: string;
    username: string;
    password: string;
}

export namespace AppConfig {

    export const getDatabase = (): IDatabaseConfig => Config.get('database');
    export const setDatabase = (config: IDatabaseConfig): Promise<void> => Config.set('database', config);

}
