import { UNPROCESSABLE_ENTITY } from 'http-status';
import { injectable } from 'inversify';
import {
    AppConfig,
} from '../config/app.config';
import { AppSetup } from '../entities/dto';
import { APIError } from '../helpers/api-error.cls';
import { container } from '../inversify.config';
import { DatabaseConnectionService } from './database-connection.service';

/**
 * Application setup service class
 */
@injectable()
export class SetupService {

    private readonly dbConnService: DatabaseConnectionService;

    constructor(dbConnService: DatabaseConnectionService) {
        this.dbConnService = dbConnService;
    }

    /**
     * Makes settings on application
     * @param setup Application setup
     */
    public async setup(setup: AppSetup): Promise<void> {
        try {
            await AppConfig.setDatabase(setup.database);
            await this.dbConnService.connect();
        } catch (err) {
            const message = typeof err === 'string' ? err : err.message || 'Application setup error.';
            const error = new APIError(message, UNPROCESSABLE_ENTITY);
            await AppConfig.setDatabase(null);

            return Promise.reject(error);
        }
    }

}

container.bind<SetupService>(SetupService)
    .to(SetupService);
