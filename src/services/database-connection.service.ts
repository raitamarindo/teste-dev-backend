import { injectable } from 'inversify';
import {
    connect,
    Mongoose,
} from 'mongoose';
import {
    AppConfig,
    IDatabaseConfig,
} from '../config/app.config';
import { container } from '../inversify.config';

/**
 * Database connection service class
 */
@injectable()
export class DatabaseConnectionService {

    public async connect(): Promise<Mongoose> {
        const config: IDatabaseConfig = AppConfig.getDatabase();

        return connect(
            `mongodb://${config.username}:${config.password}@${config.host}:${config.port}/${config.database}`,
            { useNewUrlParser: true });
    }

}

container.bind<DatabaseConnectionService>(DatabaseConnectionService).to(DatabaseConnectionService);
