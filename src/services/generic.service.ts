import { injectable } from 'inversify';
import {
    Model,
    Types,
} from 'mongoose';
import { InstanceType } from 'typegoose';
import { GenericEntity } from '../entities/entity.cls';
import { GenericFilter } from '../entities/filters';

/**
 * Generic service class
 */
@injectable()
export abstract class GenericService<T extends GenericEntity, F extends GenericFilter> {

    public async find(filter: F): Promise<T[]> {
        return this.getModel().aggregate(this.buildQuery(filter, false));
    }

    public async count(filter: F): Promise<number> {
        return this.getModel().aggregate(this.buildQuery(filter, true))
            .then((result: any) => {
                return result[0] ? result[0].count : 0;
            });
    }

    public async findById(id: string): Promise<T> {
        return this.getModel().findById(id);
    }

    public async findOne(filter: F): Promise<T> {
        filter.limit = 1;

        const entities: T[] = await this.getModel().aggregate(this.buildQuery(filter, false))

        return entities[0];
    }

    public async create(entity: T | T[]): Promise<T[]> {
        if (entity instanceof Array) {
            return this.getModel().insertMany(entity
                .map((e: T) => {
                    e.createdAt = new Date();

                    return e;
                }));
        } else {
            const stored: T = await this.getModel().create(entity)

            return [stored];
        }
    }

    public async createOrUpdate(entity: T | T[]): Promise<T[]> {
        const entitiesCreate: T[] = (entity instanceof Array ? entity : [entity])
            .filter((e: T) => !e._id);
        const entitiesUpdate: T[] = (entity instanceof Array ? entity : [entity])
            .filter((e: T) => !!e._id);

        const created: T[] = await this.create(entitiesCreate)
        const updated: T[] = await Promise.all(entitiesUpdate.map((e: T) => this.update(e)));

        return created.concat(updated);
    }

    public async update(entity: T): Promise<T> {
        const e: T = await this.findById(entity._id.toString());
        await this.getModel().update({ _id: entity._id.toString() }, { $set: entity });

        return { ...e, ...entity };
    }

    public async remove(id: string | string[]): Promise<void> {
        if (Array.isArray(id)) {
            await this.getModel().deleteMany({ _id: { $in: id.map((i: string) => new Types.ObjectId(i)) } });
        } else {
            await this.getModel().deleteOne({ _id: new Types.ObjectId(id) });
        }

    }

    protected abstract getModel(): Model<InstanceType<T>>;

    protected buildQuery(filter: F, isCount: boolean) {
        const query: any[] = [];
        const lookups = filter.getLookups();
        const unwinds = filter.getUnwinds();
        const project = filter.getProject();
        const sort = filter.getSort();
        const group = filter.getGroup();
        const replaceRoot = filter.getReplaceRoot();
        const conditions = filter.getConditions();
        if (lookups) {
            for (const lookup of lookups) {
                query.push({ $lookup: lookup });
                query.push({ $unwind: `$${lookup.as}` });
            }
        }
        if (unwinds) {
            for (const unwind of unwinds) {
                if (typeof unwind === 'string') {
                    query.push({ $unwind: `$${unwind}` });
                } else {
                    unwind.path = `$${unwind.path}`;
                    query.push({ $unwind: unwind });
                }
            }
        }
        query.push({ $match: conditions });
        if (filter.sample) {
            query.push({ $sample: { size: filter.sample } });
        }
        if (group) {
            query.push({ $group: group });
        }
        if (project) {
            query.push({ $project: project });
        }
        if (replaceRoot) {
            query.push({ $replaceRoot: { newRoot: replaceRoot } });
        }
        if (!isCount) {
            if (sort) {
                query.push({ $sort: sort });
            } else {
                query.push({ $sort: { createdAt: 1 } });
            }
            if (filter.limit != null) {
                if (filter.page == null) {
                    filter.page = 0;
                }
                query.push({ $skip: filter.page * filter.limit });
                query.push({ $limit: filter.limit });
            }
        } else {
            query.push({ $count: 'count' });
        }

        return query;
    }

}
