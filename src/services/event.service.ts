import { GenericService } from './generic.service';
import {
    Event,
    eventModel,
} from '../entities';
import { EventFilter } from '../entities/filters';
import {
    Document,
    Model,
} from 'mongoose';
import { injectable } from 'inversify';
import { container } from '../inversify.config';
import { Transaction, Product } from '../entities/dto';

/**
 * Event service class
 */
@injectable()
export class EventService extends GenericService<Event, EventFilter> {

    /**
     * Lists all event names that matches with search
     * @param search Search text on event names
     */
    public async autocomplete(search: string): Promise<string[]> {
        if (!!search && search.length > 2) {
            const filter = new EventFilter();
            filter.event = search;
            filter.setGroup({ _id: { event: '$event' } });
            filter.setProject({ event: '$_id.event' });
            const events: Event[] = await this.find(filter);

            return events.map((e: Event) => e.event);
        }

        return Promise.resolve([]);
    }

    /**
     * Lists all transactions ordered by newest timestamp
     */
    public async listTransactions(): Promise<Transaction[]> {
        const transactionMap: Map<string, Transaction> = new Map<string, Transaction>();
        const transactionIdList: string[] = [];
        const filter: EventFilter = new EventFilter();
        filter.setProject({
            timestamp: 1,
            revenue: 1,
            custom_data: 1,
        });
        const events: Event[] = await this.find(filter);
        for (const event of events) {
            const newTransaction = this.getTransactionFromEvent(event);
            const transaction = transactionMap.get(newTransaction.transaction_id);
            if (!!transaction) {
                newTransaction.products = newTransaction.products.concat(transaction.products);
                if (!newTransaction.store_name) {
                    newTransaction.store_name = transaction.store_name;
                }
                if (!newTransaction.revenue) {
                    newTransaction.revenue = transaction.revenue;
                }
            } else {
                if (transactionIdList.length > 0) {
                    for (let i = 0; i < transactionIdList.length; i++) {
                        const time = transactionMap.get(transactionIdList[i]).timestamp.getTime();
                        if (newTransaction.timestamp.getTime() > time) {
                            transactionIdList.splice(i, 0, newTransaction.transaction_id);
                            break;
                        }
                    }
                } else {
                    transactionIdList.push(newTransaction.transaction_id);
                }
            }
            transactionMap.set(newTransaction.transaction_id, newTransaction);
        }

        return transactionIdList.map((id: string) => transactionMap.get(id));
    }

    protected getModel(): Model<Event & Document> {
        return eventModel;
    }

    private getTransactionFromEvent(event: Event): Transaction {
        const transaction = new Transaction();
        const product = new Product();
        transaction.products = [];
        transaction.timestamp = new Date(event.timestamp);
        transaction.revenue = event.revenue;
        for (const d of event.custom_data) {
            switch (d.key) {
                case 'transaction_id':
                    transaction.transaction_id = d.value;
                    break;
                case 'product_name':
                    product.name = d.value;
                    break;
                case 'product_price':
                    product.price = parseFloat(d.value);
                    break;
                case 'store_name':
                    transaction.store_name = d.value;
                    break;
            }
        }
        if (!!product.name) {
            transaction.products.push(product);
        }

        return transaction;
    }

}

container.bind<EventService>(EventService).to(EventService);
