/**
 * Product class
 */
export class Product {

    public name: string;
    public price: number;

}
