import { Product } from './product.cls';

/**
 * Transaction class
 */
export class Transaction {

    public transaction_id: string;
    public timestamp: Date;
    public revenue: number;
    public store_name: string;
    public products: Product[];

}
