/**
 * Event data entry class
 */
export class EventDataEntry {

    public key: string;
    public value: string;

}
