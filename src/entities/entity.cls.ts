import { Types } from 'mongoose';
import {
    pre,
    prop,
    Typegoose,
} from 'typegoose';

@pre<GenericEntity>('save', function (next: Function) {
    // tslint:disable-next-line:no-this-assignment
    const entity: GenericEntity = this;
    if (!entity.createdAt) {
        entity.createdAt = new Date();
    }
    next();
})
export class GenericEntity extends Typegoose {

    public _id: Types.ObjectId;

    @prop()
    public createdAt?: Date;

}
