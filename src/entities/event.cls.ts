import { GenericEntity } from './entity.cls';
import { prop } from 'typegoose';
import { EventDataEntry } from './event-data-entry.cls';

export class Event extends GenericEntity {

    @prop({ required: true })
    public event: string;
    @prop({ required: true })
    public timestamp: Date;
    @prop()
    public custom_data?: EventDataEntry[];
    @prop()
    public revenue: number;

}

export const eventModel = new Event().getModelForClass(Event);
