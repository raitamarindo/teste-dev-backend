import { Types } from 'mongoose';

/**
 * Generic filter class
 */
export abstract class GenericFilter {

    public ids: string[];
    public createdBefore: Date;
    public createdAfter: Date;
    public page: number;
    public limit: number;
    public sort: any;
    public sample: number;
    private lookups: any[];
    private unwinds: any[];
    private project: any;
    private group: any;
    private replaceRoot: string;

    public static parse<F extends GenericFilter>(filterType: new () => F, query: any): F {
        const filter = new filterType();
        if (!!query) {
            for (const attr of Object.keys(query)) {
                filter[attr] = GenericFilter.parseValue(query[attr]);
            }
        }

        return filter;
    }

    protected static attachMinMaxConditions(conditions: any, attrName: string, minValue: number, maxValue?: number, value?: number) {
        if (maxValue != null) {
            conditions[attrName] = {
                $lte: maxValue,
            };
        }
        if (minValue != null) {
            const minCondition = {
                $gte: minValue,
            };
            const cMax = {};
            const cMin = {};
            if (conditions[attrName] != null) {
                if (conditions.$and == null) {
                    conditions.$and = [];
                }
                cMax[attrName] = conditions[attrName];
                conditions.$and.push(cMax);
                cMin[attrName] = minCondition;
                conditions.$and.push(cMin);
                delete conditions[attrName];
            } else {
                conditions[attrName] = minCondition;
            }
        }
        if (value !== undefined) {
            conditions[attrName] = value;
        }
    }

    protected static attachAfterBeforeConditions(conditions: any, attrName: string, afterDate: Date, beforeDate?: Date, atDate?: Date) {
        if (beforeDate != null) {
            conditions[attrName] = {
                $lt: beforeDate instanceof Date ? beforeDate : new Date(beforeDate),
            };
        }
        if (afterDate != null) {
            const afterCondition = {
                $gt: afterDate instanceof Date ? afterDate : new Date(afterDate),
            };
            const cBefore = {};
            const cAfter = {};
            if (conditions[attrName] != null) {
                if (conditions.$and == null) {
                    conditions.$and = [];
                }
                cBefore[attrName] = conditions[attrName];
                conditions.$and.push(cBefore);
                cAfter[attrName] = afterCondition;
                conditions.$and.push(cAfter);
                delete conditions[attrName];
            } else {
                conditions[attrName] = afterCondition;
            }
        }
        if (atDate !== undefined) {
            conditions[attrName] = atDate;
        }
    }

    private static parseValue(value: any) {
        let parsed = null;
        if (typeof value === 'string') {
            switch (value) {
                case 'undefined': parsed = undefined; break;
                case 'null': parsed = null; break;
                case 'true': parsed = true; break;
                case 'false': parsed = false; break;
                default:
                    if (value.match(/^\-?[0-9]+(\.[0-9]+)?$/g)) {
                        parsed = parseFloat(value);
                    } else if (value.match(/^\d{4}\-\d{2}\-\d{2}T\d{2}\:\d{2}\:\d{2}\.\d{3}Z$/g)) {
                        parsed = new Date(value);
                    } else {
                        parsed = value;
                    }
            }
        } else if (value instanceof Array) {
            parsed = [];
            for (const v of value) {
                parsed.push(GenericFilter.parseValue(v));
            }
        } else if (value instanceof Object) {
            parsed = {};
            for (const attr of Object.keys(value)) {
                parsed[attr] = GenericFilter.parseValue(value[attr]);
            }
        }

        return parsed;
    }

    public getConditions(): any {
        const conditions: any = {};
        if (this.ids != null && this.ids.length > 0) {
            conditions._id = {
                $in: this.ids.map((id: string) => new Types.ObjectId(id)),
            };
        }
        GenericFilter.attachAfterBeforeConditions(conditions, 'createdAt', this.createdAfter, this.createdBefore);
        this.attachConditions(conditions);

        return conditions;
    }

    public getLookups(): any[] {
        return this.lookups || this.setupLookups();
    }

    public setLookups(lookups: any[]): void {
        this.lookups = lookups;
    }

    public getUnwinds(): any[] {
        return this.unwinds || this.setupUnwinds();
    }

    public setUnwinds(unwinds: any[]): void {
        this.unwinds = unwinds;
    }

    public getProject(): any {
        return this.project || this.setupProject();
    }

    public setProject(project: any): void {
        this.project = project;
    }

    public getGroup(): any {
        return this.group || this.setupGroup();
    }

    public setGroup(group: any) {
        this.group = group;
    }

    public getReplaceRoot(): string {
        return this.replaceRoot;
    }

    public setReplaceRoot(replaceRoot: string): void {
        this.replaceRoot = replaceRoot;
    }

    public getSort(): any {
        return this.sort || this.setupSort() || undefined;
    }

    protected setupLookups(): any[] {
        return [];
    }

    protected setupUnwinds(): string[] {
        return [];
    }

    protected setupGroup(): any {
        //
    }

    protected setupProject(): any {
        //
    }

    protected setupSort(): any {
        //
    }

    protected abstract attachConditions(conditions: any): void;

}
