import { GenericFilter } from './generic.filter';

export class EventFilter extends GenericFilter {

    public event: string;

    protected attachConditions(conditions: any): void {
        if (this.event !== undefined) {
            conditions.event = this.event == null ? null : new RegExp(`^${this.event}.*`, 'i');
        }
    }

    protected setupProject() {
        return {
            event: 1,
            timestamp: 1,
        };
    }

}