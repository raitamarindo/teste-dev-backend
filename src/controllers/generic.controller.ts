import {
    Request,
    Response,
} from 'express';
import {
    CREATED,
    NOT_FOUND,
    OK,
} from 'http-status';
import { GenericEntity } from '../entities';
import { GenericFilter } from '../entities/filters';
import { APIError } from '../helpers/api-error.cls';
import { GenericService } from '../services';

export interface IFindResult<T extends GenericEntity, F extends GenericFilter> {
    entities: T[];
    filter: F;
    total: number;
}

/**
 * Generic entity controller
 */
export abstract class GenericController<T extends GenericEntity, F extends GenericFilter> {

    public abstract read(req: Request, res: Response, id: string): Promise<Response>;

    public abstract list(req: Request, res: Response): Promise<Response>;

    public abstract create(req: Request, res: Response, entity: T): Promise<Response>;

    public abstract update(req: Request, res: Response, id: string, entity: T): Promise<Response>;

    public abstract remove(req: Request, res: Response, id: string): Promise<Response>;

    protected abstract getService(): GenericService<T, F>;

    protected abstract getFilterType(): new () => F;

    /**
     * Retrieves an entity by its id
     * @param req Http request
     * @param res Http response
     * @param id Entity id
     */
    protected async defaultRead(_req: Request, res: Response, id: string): Promise<Response> {
        const entity: T = await this.getService()
            .findById(id);
        if (!entity) {
            throw new APIError('Data not found', NOT_FOUND);
        } else {
            return res.status(OK)
                .json(entity);
        }
    }

    /**
     * List entities by filter
     * @param req Http request
     * @param res Http response
     */
    protected async defaultList(req: Request, res: Response): Promise<Response> {
        const filter: F = GenericFilter.parse<F>(this.getFilterType(), req.query);
        const result: IFindResult<T, F> = {
            entities: await this.getService().find(filter),
            filter: filter,
            total: await this.getService().count(filter),
        };

        return res.status(OK)
            .json(result);
    }

    /**
     * Creates one or more entities
     * @param req Http request
     * @param res Http response
     */
    protected async defaultCreate(_req: Request, res: Response, entity: T | T[]): Promise<Response> {
        const createdEntities: T[] = await this.getService()
            .create(entity);

        return res.status(CREATED)
            .json(createdEntities);
    }

    /**
     * Updates an entity
     * @param req Http request
     * @param res Http response
     * @param id Entity id
     */
    protected async defaultUpdate(_req: Request, res: Response, id: string, entity: T): Promise<Response> {
        const updatedEntity: T = await this.getService()
            .update({ ...entity, _id: id });

        return res.status(OK)
            .json(updatedEntity);
    }

    /**
     * Removes an entity by its id
     * @param req Http request
     * @param res Http response
     * @param id Entity id
     */
    protected async defaultRemove(_req: Request, res: Response, id: string): Promise<Response> {
        const entity: T = await this.getService()
            .findById(id);
        if (!entity) {
            throw new APIError('Data not found', NOT_FOUND);
        }
        await this.getService()
            .remove(id);

        return res.status(OK)
            .send();
    }

}
