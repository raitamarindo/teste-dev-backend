import { Request, Response } from 'express';
import { JsonController, Get, Req, Res } from 'routing-controllers';
import { EventService } from '../services';
import { Transaction } from '../entities/dto';
import { OK } from 'http-status';
import { container } from '../inversify.config';

@JsonController('/transactions')
export class TransactionController {

    @Get('/timeline')
    public async timeline(@Req() _req: Request, @Res() res: Response): Promise<Response> {
        const transactions: Transaction[] = await this.getEventService().listTransactions();

        return res.status(OK)
            .json({ timeline: transactions });
    }

    /**
     * Get an instance of Event service
     */
    protected getEventService(): EventService {
        return container.get<EventService>(EventService);
    }

}