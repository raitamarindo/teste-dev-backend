import * as bodyParser from 'body-parser';
import { Request, Response } from 'express';
import { JsonController, Get, Req, Res, Param, Post, Body, UseBefore } from 'routing-controllers';
import { GenericController } from './generic.controller';
import { EventFilter } from '../entities/filters';
import { Event } from '../entities';
import { EventService } from '../services';
import { container } from '../inversify.config';
import { OK } from 'http-status';

/**
 * Event controller class
 */
@JsonController('/events')
export class EventController extends GenericController<Event, EventFilter> {

    public update(_req: Request, _res: Response, _id: string, _entity: Event): Promise<Response> {
        throw new Error('Method not implemented.');
    }

    public remove(_req: Request, _res: Response, _id: string): Promise<Response> {
        throw new Error('Method not implemented.');
    }

    /**
     * Get Events for autocomplete
     * @param req Http request
     * @param res Http response
     */
    @Get('/autocomplete/:search')
    public async autocomplete(@Req() _req: Request, @Res() res: Response, @Param('search') search: string): Promise<Response> {
        const events: string[] = await this.getService().autocomplete(search);

        return res.status(OK)
            .json(events);
    }

    /**
     * Gets one Event by id
     * @param req Http request
     * @param res Http response
     * @param id Event id
     */
    @Get('/:id')
    public read(@Req() req: Request, @Res() res: Response, @Param('id') id: string): Promise<Response> {
        return super.defaultRead(req, res, id);
    }

    /**
     * Lists Events by filter
     * @param req Http request
     * @param res Http response
     */
    @Get('/')
    @UseBefore(bodyParser.urlencoded({ extended: true }))
    public list(@Req() req: Request, @Res() res: Response): Promise<Response> {
        return super.defaultList(req, res);
    }

    /**
     * Creates one or more Events
     * @param req Http request
     * @param res Http response
     * @param Event Event(s) to be creates
     */
    @Post('/')
    public create(@Req() req: Request, @Res() res: Response, @Body() Event: Event): Promise<Response> {
        return super.defaultCreate(req, res, Event);
    }

    /**
     * Get an instance of Event service
     */
    protected getService(): EventService {
        return container.get<EventService>(EventService);
    }

    /**
     * Gets Event filter class constructor
     */
    protected getFilterType(): new () => EventFilter {
        return EventFilter;
    }


}
